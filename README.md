# Advent Of Code 2023

Developed in Python 3.11

https://adventofcode.com/2023

I like writing nice code. You will not find the most performant code (fastest, least amount of lines) in this repo but 
it will be readable and expandable without compromise :D 
