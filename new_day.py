import re
import sys
from datetime import date
from pathlib import Path

import browser_cookie3
import requests as requests
from markdownify import markdownify

YEAR: int = 2023
DIRECTORY = Path(r"./src")

# Fetching the day
if len(sys.argv) > 1:
    day = int(sys.argv[1])
else:
    day = date.today().strftime("%d").lstrip("0")

folder = (DIRECTORY / f'day{day}')


def initial_creation():
    folder.mkdir()
    (folder / 'test.txt').touch()

    python = (folder / 'main.py')
    python.touch()

    with open(python, 'w') as fid:
        fid.write("""
from typing import *        
from dataclasses import dataclass, field
from util import Input
from util import StringExtractor
from util import time_function


@time_function
def solve(filename: str):
    input = Input(filename).read()
    print(input)
    

if __name__ == '__main__':
    solve('test.txt')""")

    input = (folder / 'input.txt')
    input.touch()

    with open(input, 'w') as fid:
        cookie = browser_cookie3.firefox(domain_name='adventofcode.com')
        r = requests.get(f"https://adventofcode.com/{YEAR}/day/{day}/input", cookies=cookie)
        fid.write(r.text)


def store_challenge():
    challenge = (folder / 'challenge.md')
    challenge.touch()

    with open(challenge, 'w') as fid:
        cookie = browser_cookie3.firefox(domain_name='adventofcode.com')
        r = requests.get(f"https://adventofcode.com/{YEAR}/day/{day}", cookies=cookie)

        for g in re.finditer(pattern=r"(<article[\S\s]*?<\/article>)", string=r.text):
            fid.write(markdownify(g.group(0)))


if __name__ == '__main__':

    try:
        initial_creation()
    except FileExistsError as e:
        print('The directory is already existing; only updating the challenge.md file')

    store_challenge()
