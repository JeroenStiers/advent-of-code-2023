import regex as re
from dataclasses import dataclass
from typing import Sequence, Iterator


@dataclass
class Pattern:
    pattern: str

@dataclass
class DigitsPattern(Pattern):
    pattern: str = r'(\d+)'


@dataclass
class StringExtractor:

    def __init__(self, pattern: str | type[Pattern]):
        if isinstance(pattern, type):
            self.pattern = re.compile(pattern().pattern)
        else:
            self.pattern = re.compile(pattern)

    def __str__(self) -> str:
        return f"StringExtractor(pattern={self.pattern})"

    def extract(self, input: str) -> bool | Sequence[str]:
        matches = self.pattern.match(input)
        if matches is None:
            return False
        return matches.groups()

    def finditer(self, input: str) -> Iterator[re.Match]:
        return self.pattern.finditer(input)

    def findall(self, input: str, overlapped: bool = False) -> list[re.Match]:
        return self.pattern.findall(input, overlapped=overlapped)
