from typing import *
from copy import deepcopy
from dataclasses import dataclass, field
from src.util.util import Position, GridExtent


@dataclass
class Grid:
    grid: List[List[int]] = field(default_factory=list)
    width: int = 0
    height: int = 0

    @staticmethod
    def initialise(width: int, height: int, value: int = 0) -> "Grid":
        grid = Grid()
        for r in range(height):
            grid.add_row([value for _ in range(width)])
        return grid

    def add_row(self, row: Iterable[int]):
        if isinstance(row, Iterable):
            row = list(row)

        if 0 < self.width != len(row):
            raise ValueError(f"The provided row has a length of {len(row)} while the grid has got a width of {self.width}")

        self.width = len(row)
        self.grid.append(row)
        self.height = len(self.grid)

    def clone(self) -> "Grid":
        return deepcopy(self)

    def print(self) -> None:
        print(f"\n{self.width}x{self.height}")
        print("----")
        for r in self.grid:
            print(''.join(map(str, r)))
        print()

    def find_value(self, value) -> list[Position]:
        to_return: list[Position] = []
        for position in self.get_positions():
            if self.get_value(position=position) == value:
                to_return.append(position)
        return to_return

    def get_value(self, position: Position):
        if position.row >= self.height or position.col >= self.width:
            raise ValueError(f"The provided position {position} is not located within the grid.")
        return self.grid[position.row][position.col]

    def get_row_indices(self) -> Iterable[int]:
        for r in range(self.height):
            yield r

    def get_col_indices(self) -> Iterable[int]:
        for c in range(self.width):
            yield c

    def get_row(self, index: int) -> Iterable[int]:
        if index >= self.height:
            raise ValueError(f"The requested row '{index}' is not existing in the grid. The max row index is {self.height - 1}")

        for i in self.grid[index]:
            yield i

    def get_col(self, index: int) -> Iterable[int]:
        if index >= self.width:
            raise ValueError(f"The requested column '{index}' is not existing in the grid. The max column index is {self.width - 1}")

        for r in self.grid:
            yield r[index]

    def get_positions(self, extent: GridExtent | None = None) -> Iterable[Position]:
        if extent is None:
            extent = GridExtent(min_col=0, min_row=0, max_col=self.width-1, max_row=self.height-1)
        for p in extent.iterate_positions():
            yield p

    def get_values(self) -> Iterable[int]:
        for position in self.get_positions():
            yield self.get_value(position=position)

    def get_edge_positions(self) -> Iterable[Position]:
        for r in range(self.height):
            yield Position(col=0, row=r)
            yield Position(col=self.width-1, row=r)

        for c in range(self.width):
            yield Position(col=c, row=0)
            yield Position(col=c, row=self.height-1)

    def set_value(self, value: int, positions: Iterable[Position]):
        for p in positions:
            self.grid[p.row][p.col] = value


if __name__ == '__main__':
    t = Grid()
    c = t.clone()

    t = Grid.initialise(3, 5, 0)

    t.print()
    t.set_value(1, t.get_edge_positions())
    t.print()
    print('\n'.join([str(i) for i in t.get_edge_positions()]))