from pathlib import Path
from typing import List


class Input:

    def __init__(self, filename='input.txt'):
        self.filename: str = filename

    def read(self, convert_to_int: bool = False, remove_emtpy_lines: bool = True):
        with open(Path.cwd() / self.filename, 'r') as fid:
            lines = fid.read().splitlines()

        if remove_emtpy_lines:
            lines = [l for l in lines if len(l) > 0]

        if convert_to_int:
            return map(lines, int)
        return lines

    @staticmethod
    def define_groups(lines=None, separator="", concatenate_group=False):
        """
        Combines a list of features to groups based on a separator. The resulting groups will be returned as a list of
        lists if concatenate_group = False or otherwise as a list of strings (different lines between one group are
        joined by a " "

        :param lines: list
        :param separator: character that defines a new group
        :param concatenate_group: boolean that indicates whether one group should consist of a string or a list itself
        :return:
        """
        result = []
        lines = lines or []

        while len(lines) > 0:
            group = []
            a = lines[0]
            while len(lines) > 0 and lines[0] != separator:
                group.append(lines[0])
                lines.pop(0)

            if len(lines) > 0:
                lines.pop(0)

            if concatenate_group:
                result.append(' '.join(group))
            else:
                result.append(group)

        return result
