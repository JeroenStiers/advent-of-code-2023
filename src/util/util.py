from dataclasses import dataclass, field
from itertools import product
from typing import Iterable


@dataclass
class Position:
    col: int
    row: int

    def __eq__(self, other: "Position") -> bool:
        return self.col == other.col and self.row == other.row

    def __hash__(self) -> int:
        return hash((self.col, self.row))

    def clone(self) -> "Position":
        return Position(col=self.col, row=self.row)


@dataclass
class Size:
    width: int
    height: int

    def __eq__(self, other: "Size") -> bool:
        return self.width == other.width and self.height == other.height


@dataclass
class Velocity:
    dx: int
    dy: int

    def __eq__(self, other: "Velocity") -> bool:
        return self.dx == other.dx and self.dy == other.dy


@dataclass
class GridExtent:
    min_col: int
    min_row: int
    max_col: int
    max_row: int

    @property
    def area(self) -> int:
        return (self.max_col - self.min_col) * (self.max_row - self.min_row)

    def iterate_positions(self) -> Iterable[Position]:
        for col, row in product(range(self.min_col, self.max_col + 1), range(self.min_row, self.max_row + 1)):
            yield Position(col=col, row=row)

