import logging
from dataclasses import dataclass, field
from typing import List, Iterable
from src.util.util import Position, Size, Velocity, GridExtent

logger = logging.getLogger(__name__)


@dataclass
class GridAgent:
    position: Position
    size: Size = field(default_factory=Size(1, 1))
    velocity: Velocity = field(default_factory=Velocity(0, 0))
    name: str = ''

    def update(self, nb_steps: int = 1) -> "GridAgent":
        self.position.col += self.velocity.dx * nb_steps
        self.position.row += self.velocity.dy * nb_steps
        return self

    @property
    def extent(self) -> GridExtent:
        return GridExtent(min_col=self.position.col, min_row=self.position.row, max_col=self.position.col + self.size.width - 1,
                          max_row=self.position.row + self.size.height - 1)


@dataclass
class GridSpace:
    """
    Defines a 2D grid having a list of agents on it that are able to move.
    X: rises to the right
    Y: rises to the bottom
    """
    __agents: List[GridAgent] = field(default_factory=lambda: [])
    __time: int = 0

    @property
    def extent(self) -> GridExtent:
        extent = GridExtent(min_col=-1, min_row=-1, max_col=-1, max_row=-1)
        for agent in self.__agents:
            if extent.min_col == -1 or agent.position.col < extent.min_col:
                extent.min_col = agent.position.col
            if extent.max_col == -1 or agent.position.col > extent.max_col:
                extent.max_col = agent.position.col
            if extent.min_row == -1 or agent.position.row < extent.min_row:
                extent.min_row = agent.position.row
            if extent.max_row == -1 or agent.position.row > extent.max_row:
                extent.max_row = agent.position.row
        return extent

    def add_agent(self, agent: GridAgent):
        self.__agents.append(agent)

    @property
    def agents(self) -> List[GridAgent]:
        return self.__agents

    def update(self, nb_steps: int = 1):
        for agent in self.__agents:
            agent.update(nb_steps)
        self.__time += nb_steps

    def __position_occupied(self, position: Position) -> bool:
        return any([a.position == position for a in self.__agents])

    def __reset(self):
        if self.__time != 0:
            self.update(nb_steps=self.__time * -1)

    def agents_inline(self, min_length: int = 4) -> bool:
        for agent in self.__agents:
            fake_agent = GridAgent(position=agent.position.clone(), velocity=Velocity(0, 1))
            for iteration in range(min_length):
                fake_agent.update()
                if not self.__position_occupied(position=fake_agent.position):
                    break
            else:
                # Whenever we exited the for loop without hitting a break statement
                # (so when a straight line of the defined length was found)
                return True
        return False

    def find_time_with_minimum_area(self) -> int:
        """This methods performs a series of optimised steps so get the timestamp when the grid has got the minimum area"""
        self.__reset()
        logger.info("Finding the time when the grid has the minimum area.")
        for step_size in (1000, 250, 100, 25, 5, 1):
            logger.debug(f"Using a step_size of {step_size} to analyse from time {self.__time}")

            area: int = self.extent.area
            while True:
                self.update(nb_steps=step_size)
                logger.debug(f"\t analysing {self.__time}")

                if (new_area := self.extent.area) > area:
                    # Whenever the new area is bigger than the last one, we are past the minimum
                    # Revert 2 step_sizes to start an analysis with a finer step_size
                    self.update(nb_steps=-2 * step_size)
                    break
                area = new_area
        self.update(nb_steps=step_size)
        logger.info(f"At timestamp {self.__time} the Grid has got the minimum extent.")
        return self.__time

    def print(self):
        extent = self.extent
        for row in range(extent.min_row, extent.max_row + 1, 1):
            for col in range(extent.min_col, extent.max_col + 1, 1):
                pos = Position(col, row)
                if self.__position_occupied(pos):
                    print('#', end='')
                else:
                    print('.', end='')
            print()

    def write_to_textfile(self, filename: str = 'output.txt'):
        extent = self.extent
        with open(filename, 'w') as fid:
            for row in range(extent.min_row, extent.max_row + 1, 1):
                for col in range(extent.min_col, extent.max_col + 1, 1):
                    pos = Position(col, row)
                    if self.__position_occupied(pos):
                        fid.write("#")
                    else:
                        fid.write('.')
                fid.write('\n')
