from enum import Enum

from pydantic import BaseModel


class Direction(Enum):
    LEFT = 'L'
    RIGHT = 'R'


class Node(BaseModel):
    name: str | int | float
    left: "Node" = None
    right: "Node" = None

    @property
    def is_leaf(self) -> bool:
        return ((self.left is None and self.right is None) or
                (self.name == self.left.name and self.name == self.right.name))


class BinaryTree:

    def __init__(self):
        self.nodes: dict[str, Node] = {}

    def add_node(self, name: str, left: str = None, right: str = None, is_start: bool = False):
        if left in self.nodes:
            left = self.nodes[left]
        else:
            left = Node(name=left)
            self.nodes[left.name] = left

        if right in self.nodes:
            right = self.nodes[right]
        else:
            right = Node(name=right)
            self.nodes[right.name] = right

        if name in self.nodes:
            node = self.nodes[name]
        else:
            node = Node(name=name)
            self.nodes[name] = node

        node.left = left
        node.right = right

        if is_start:
            if is_start is not None:
                raise ValueError(f"A start_node was already defined. You can only have 1 start node")
            self.start_node_name = name

    def get_node_with_name(self, name: str) -> Node:
        return self.nodes[name]

    def navigate(self, from_: str | Node, direction: str | Direction):
        if isinstance(from_, str):
            from_ = self.nodes[from_]

        if isinstance(direction, str):
            direction = Direction(direction)

        if from_.is_leaf:
            raise ValueError(f"You cannot navigate a leaf (end) node")

        if direction == Direction.LEFT:
            return from_.left
        if direction == Direction.RIGHT:
            return from_.right


if __name__ == '__main__':
    t = BinaryTree()
    t.add_node(name='A', left='B', right='C')
    t.add_node(name='B', left='A', right='C')
    t.add_node(name='C', left='B', right='Z')

