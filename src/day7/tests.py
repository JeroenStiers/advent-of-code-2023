from src.day7.main import Card, Type


def test_card_order():
    two = Card('2')
    king = Card('K')
    assert king > two
    assert king != two
