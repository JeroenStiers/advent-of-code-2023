from pydantic import BaseModel
from functools import lru_cache
from collections import Counter
from enum import Enum
from typing import *
from dataclasses import dataclass, field
from util import Input
from util import StringExtractor
from util import time_function


class Card(Enum):
    JACK = 'J'
    TWO = '2'
    TRHEE = '3'
    FOUR = '4'
    FIVE = '5'
    SIX = '6'
    SEVEN = '7'
    EIGHT = '8'
    NINE = '9'
    TEN = 'T'
    QUEEN = 'Q'
    KING = 'K'
    ACE = 'A'

    def __repr__(self) -> str:
        return self.value

    @classmethod
    @lru_cache()
    def members(cls):
        return list(cls)

    def __lt__(self, other) -> bool:
        return self.members().index(self) < self.members().index(other)


class Type(Enum):
    HIGH_CARD = 'High Card'
    ONE_PAIR = 'One Pair'
    TWO_PAIR = 'Two Pair'
    THREE_OF_A_KIND = 'Three of a kind'
    FULL_HOUSE = 'Full House'
    FOUR_OF_A_KIND = 'Four of a kind',
    FIVE_OF_A_KIND = 'Five of a kind'

    @classmethod
    @lru_cache
    def members(cls):
        return list(cls)

    def __lt__(self, other) -> bool:
        return self.members().index(self) < self.members().index(other)


class Hand(BaseModel):
    cards: List[Card]
    bid: int

    def __str__(self) -> str:
        return f"cards={self.cards}, bid={self.bid}, type={self.type}"

    def __repr__(self) -> str:
        return str(self)

    @property
    def type(self) -> Type:
        cards = [c for c in self.cards if c is not Card.JACK]
        count_jokers: int = 5 - len(cards)
        if count_jokers == 5:
            return Type.FIVE_OF_A_KIND

        counts = sorted([c for e, c in Counter(cards).items()], reverse=True)
        if counts[0] + count_jokers == 5:
            return Type.FIVE_OF_A_KIND
        if counts[0] + count_jokers == 4:
            return Type.FOUR_OF_A_KIND
        if counts[0] + count_jokers == 3 and counts[1] == 2:
            return Type.FULL_HOUSE
        if counts[0] + count_jokers == 3:
            return Type.THREE_OF_A_KIND
        if counts[0] + count_jokers == 2 and counts[1] == 2:
            return Type.TWO_PAIR
        if counts[0] + count_jokers == 2:
            return Type.ONE_PAIR
        return Type.HIGH_CARD

    def __eq__(self, other) -> bool:
        return self.cards == other.cards

    def __lt__(self, other) -> bool:
        if self.type is not other.type:
            return self.type < other.type

        for i, j in zip(self.cards, other.cards):
            if i is not j:
                return i < j
        return False


def parse(input: list[str]) -> List[Hand]:
    re = StringExtractor(pattern="^([2-9TJQKA]{5}) (\d+)$")
    hands: List[Hand] = []

    for i in input:
        cards, bid = re.extract(input=i)
        hands.append(Hand(
            cards=list(map(Card, cards)),
            bid=int(bid),
        ))
    return hands


@time_function
def solve(filename: str):
    input = Input(filename).read()
    hands = sorted(parse(input))
    return sum([(1+i)*h.bid for i, h in enumerate(hands)])


if __name__ == '__main__':
    print(solve('input.txt'))
