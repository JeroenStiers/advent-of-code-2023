from typing import *
from itertools import cycle
from math import gcd
from util import Input
from util import StringExtractor
from util import time_function
from util.binarytree import BinaryTree, Node


def build_tree(input: list[str]) -> BinaryTree:
    t = BinaryTree()
    start_nodes: list[str] = []
    re = StringExtractor(pattern="^([0-9A-Z]{3}) = \(([0-9A-Z]{3}), ([0-9A-Z]{3})\)$")

    for i in input:
        name, left, right = re.extract(i)
        t.add_node(name=name, left=left, right=right)

        if name[-1] == 'A':
            start_nodes.append(name)

    return t, start_nodes


@time_function
def solve(filename: str, start_nodes: list[str] | None = None):
    input = Input(filename).read()
    directions = cycle([i for i in input[0]])
    tree, current_nodes = build_tree(input[1:])

    if start_nodes is not None and len(start_nodes) > 0:
        current_nodes = start_nodes

    count_steps: int = 0
    repeats = {i: 0 for i in range(len(current_nodes))}

    while True:
        direction = next(directions)
        new_nodes = []
        for node in current_nodes:
            new_nodes.append(tree.navigate(from_=node, direction=direction))
        count_steps += 1
        current_nodes = new_nodes

        for i, n in enumerate(current_nodes):
            if n.name[-1] == 'Z' and repeats[i] == 0:
                repeats[i] = count_steps

        if sorted(repeats.values())[0] != 0:
            break

    repeats: list[int] = list(repeats.values())
    biggest_repeat = max(repeats)
    repeats = sorted(repeats, reverse=True)[1:]

    step: int = 0
    while True:
        step += 1

        if step % 10_000_000 == 0:
            print(step)

        result = step * biggest_repeat
        for i in repeats:
            if result % i != 0:
                break
        else:
            # Whenever no break was encountered in the for loop - so when we found the answer :)
            break

    return result

    # return gcd(*[int(i) for i in repeats.values()])


if __name__ == '__main__':
    print(solve('input.txt', start_nodes=['AAA']))
    print(solve('input.txt'))
