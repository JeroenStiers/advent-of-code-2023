from util import time_function


def simulate_race(race_duration: int, minimum_distance: int) -> int:
    # Simulate the possible distances to get in the race given the race_duration
    count_viable_options: int = 0
    for charging_time in range(race_duration + 1):
        if charging_time * (race_duration-charging_time) > minimum_distance:
            count_viable_options += 1
    return count_viable_options


@time_function
def solve(filename: str):
    # input = Input(filename).read()
    # print(input)

    answer: int = 1
    races: list[tuple[int, int]] = [(7, 9,), (15, 40), (30, 200), ]
    races = [(49, 298,), (78, 1185,), (79, 1066,), (80, 1181,), ]

    # part 2
    races = [(71530, 940200,)]
    races = [(49787980, 298118510661181)]

    for race_duration, minimum_distance in races:
        print(f"Calculating the reachable distances for a {race_duration=}")
        viable_configurations = simulate_race(race_duration=race_duration, minimum_distance=minimum_distance)
        print(f"Number of {viable_configurations=}")
        answer *= viable_configurations

    return answer


if __name__ == '__main__':
    print(solve('test.txt'))
