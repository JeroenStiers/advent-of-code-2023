from pydantic import BaseModel
from abc import ABC, abstractmethod
from typing import *
from dataclasses import dataclass, field
from util import Input, Grid, Position, GridExtent
from util import StringExtractor
from util import time_function


class Connector(BaseModel):
    row_delta: int
    col_delta: int

    def __hash__(self):
        return hash((self.row_delta, self.col_delta))

    def connects_to(self, from_: Position) -> Position:
        new_position = from_.clone()
        new_position.row += self.row_delta
        new_position.col += self.col_delta
        return new_position


class PipeType(BaseModel):
    connectors: tuple[Connector, Connector]
    value: str

    def __str__(self) -> str:
        return f"{self.value}"

    def __hash__(self):
        return hash(self.connectors)

    @classmethod
    def from_str(cls, value: str) -> "PipeType":
        return {
            '|': PipeType(value='|', connectors=(Connector(row_delta=-1, col_delta=0), Connector(row_delta=1, col_delta=0))),
            '-': PipeType(value='-', connectors=(Connector(row_delta=0, col_delta=-1), Connector(row_delta=0, col_delta=1))),
            'F': PipeType(value='F', connectors=(Connector(row_delta=1, col_delta=0), Connector(row_delta=0, col_delta=1))),
            '7': PipeType(value='7', connectors=(Connector(row_delta=0, col_delta=-1), Connector(row_delta=1, col_delta=0))),
            'J': PipeType(value='J', connectors=(Connector(row_delta=-1, col_delta=0), Connector(row_delta=0, col_delta=-1))),
            'L': PipeType(value='L', connectors=(Connector(row_delta=0, col_delta=1), Connector(row_delta=-1, col_delta=0))),
            'S': None,
        }[value]


class Pipe(BaseModel):
    type: PipeType | None
    position: Position

    def __str__(self) -> str:
        return f"Pipe({self.type}, ({self.position.row},{self.position.col}))"

    def __hash__(self):
        return hash((self.type, self.position))

    def get_connecting_pipes(self, grid: Grid) -> set["Pipe"]:
        position_side_1: Position = self.type.connectors[0].connects_to(from_=self.position)
        position_side_2: Position = self.type.connectors[1].connects_to(from_=self.position)

        if grid.get_value(position_side_1) == 'S':
            return {Pipe(type=PipeType.from_str(grid.get_value(position=position_side_2)), position=position_side_2)}

        if grid.get_value(position_side_2) == 'S':
            return {Pipe(type=PipeType.from_str(grid.get_value(position=position_side_1)), position=position_side_1)}

        return {
            Pipe(type=PipeType.from_str(grid.get_value(position=position_side_1)), position=position_side_1),
            Pipe(type=PipeType.from_str(grid.get_value(position=position_side_2)), position=position_side_2)
        }


@time_function
def solve(filename: str, start_pipe: Pipe | None = None):
    input = Input(filename).read()
    grid = Grid(width=len(input[0]), height=len(input))
    for r in input:
        grid.add_row(row=r)

    grid.print()

    if start_pipe is None:
        start_position: Position = grid.find_value(value='S')[0]
        print(
            f"Please provide a value for start_pipe. e.g.:\nPipe(type=PipeType.from_str('UPDATE'), position=Position({start_position.row}, {start_position.col}))")
        return

    pipe1, pipe2 = start_pipe.get_connecting_pipes(grid=grid)
    current_positions: set[Position] = {pipe1.position, pipe2.position}
    previous_positions: set[Position] = {start_pipe.position}

    iterations: int = 0
    while True:

        new_pipes = pipe1.get_connecting_pipes(grid=grid)
        new_pipes = new_pipes.union(pipe2.get_connecting_pipes(grid=grid))

        # remove the pipes on the positions that were visited previously
        new_pipes = {p for p in new_pipes if p.position not in previous_positions}

        print([str(p) for p in new_pipes])

        if len(new_pipes) == 1:
            # Nice, we reached the same pipe from both sides!
            break

        pipe1 = new_pipes.pop()
        pipe2 = new_pipes.pop()

        iterations += 1
        if iterations > 100:
            break

        previous_positions: set[Position] = current_positions
    return iterations


if __name__ == '__main__':

    filename: str = 'input.txt'
    # start_pipe: Pipe = Pipe(type=PipeType.from_str('UPDATE'), position=Position(49, 96))
    start_pipe = None

    if True:
        filename: str = "test.txt"
        start_pipe: Pipe = Pipe(type=PipeType.from_str('F'), position=Position(1, 1))

    print(solve(filename, start_pipe=start_pipe))
