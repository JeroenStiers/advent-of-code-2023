from typing import *
from functools import lru_cache
from dataclasses import dataclass, field
from util import Input
from util.string_extractor import StringExtractor, DigitsPattern
from util import time_function

digits_extractor = StringExtractor(pattern=DigitsPattern)


@dataclass
class Convertion:
    source_from: int
    source_to: int
    target_from: int

    def __repr__(self) -> str:
        return f"<CONVERTION: source: {self.source_from}-{self.source_to}, target: {self.target_from}>"

    def __hash__(self):
        return hash((self.source_from, self.source_to, self.target_from))


class Convertors(list):

    def __hash__(self):
        return hash(i for i in self)


def interpret_convertors(convertors: list[str]) -> Convertors:
    result: Convertors = Convertors()
    for c in convertors:
        target_from, source_from, source_len = digits_extractor.findall(c)
        result.append(Convertion(
            source_from=int(source_from),
            source_to=int(source_from) + int(source_len) - 1,
            target_from=int(target_from)
        ))
    return result


@lru_cache
def convert_item(data: int, convertors: list[Convertion]) -> int:
    for c in convertors:
        if c.source_from <= data <= c.source_to:
            return data - c.source_from + c.target_from
    return data


def convert_all(data: list[int], converter_info: list[str]) -> List[int]:
    new_data: list[int] = []

    # Interpret the convertion to be applied
    print(f"Converting using the {converter_info[0][:-1]}")
    convertors: Convertors = interpret_convertors(converter_info[1:])

    # Update the data using the provided converter
    for d in data:
        new_data.append(convert_item(data=d, convertors=convertors))
    return new_data


@time_function
def solve(filename: str, input_is_a_range: bool = False) -> list[int]:
    input = Input.define_groups(Input(filename).read(remove_emtpy_lines=False))

    # Start with fetching the data - initially the seeds
    data: list[int] = list(map(int, digits_extractor.findall(input[0][0])))

    if input_is_a_range:
        new_data: list[int] = []
        for start, length in zip(data[:-1:2], data[1::2]):
            for i in range(start, start + length):
                new_data.append(i)
        data = new_data

    # Go over each map and update the data
    for convertor in input[1:]:
        data = convert_all(data=data, converter_info=convertor)

    return data


if __name__ == '__main__':
    filename: str = 'test.txt'
    print(min(solve(filename=filename, input_is_a_range=False)))
    print(min(solve(filename=filename, input_is_a_range=True)))
