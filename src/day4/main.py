from typing import *
from math import pow
from dataclasses import dataclass, field
from util import Input
from util import StringExtractor
from util import time_function

re_card_number = StringExtractor(pattern='^Card +(\d+):')
re_numbers = StringExtractor(pattern='(\d+)')


def calculate_score_for_card(card: str):
    card_number = re_card_number.extract(card)[0]

    winning_numbers, having_numbers = card.split('|')
    winning_numbers = set([int(i) for i in re_numbers.findall(winning_numbers.split(':')[1], overlapped=False)])
    having_numbers = set([int(i) for i in re_numbers.findall(having_numbers, overlapped=False)])

    overlapping_numbers = winning_numbers.intersection(having_numbers)
    if len(overlapping_numbers) == 0:
        return 0
    return int(pow(2, len(overlapping_numbers) - 1))


@time_function
def solve(filename: str):
    input = Input(filename).read()
    score: int = 0

    for card in input:
        score = score + calculate_score_for_card(card)
    return score


if __name__ == '__main__':
    print(solve('test.txt'))
