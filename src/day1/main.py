from typing import *
from dataclasses import dataclass, field
from util import Input
from util import StringExtractor
from util import time_function

DIGITS: list[str] = [None, "one", "two", "three", "four", "five", "six", "seven", "eight", "nine"]


def convert(input: str) -> str:
    if input.isdigit():
        return input
    return DIGITS.index(input)


def clean_input_data(input: list[str]) -> list[str]:
    result: list[str] = []

    re = StringExtractor(pattern=r"(\d|one|two|three|four|five|six|seven|eight|nine)")
    for i in input:
        occurences = re.findall(i, overlapped=True)
        result.append(f"{convert(occurences[0])}{convert(occurences[-1])}")
    return result


@time_function
def solve(filename: str, input_cleaner=None):
    input = Input(filename).read()

    if input_cleaner is not None:
        input = input_cleaner(input)
    sum: int = 0

    for line in input:
        digits = [d for d in line if d.isdigit()]
        sum += int(f"{digits[0]}{digits[-1]}")
    return sum


if __name__ == '__main__':
    print(f"Part 1: {solve('input.txt')}")
    print(f"Part 2: {solve('input.txt', clean_input_data)}")
