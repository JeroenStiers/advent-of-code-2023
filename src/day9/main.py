from typing import *
from dataclasses import dataclass, field
from multiprocessing import Pool
from util import Input
from util import StringExtractor
from util import time_function

REGEX = StringExtractor(pattern="([-0-9]+)")


def extrapolate(history: str) -> tuple[int, int]:
    memory: list[tuple[int, int]] = []
    sequence = list(map(int, REGEX.findall(history)))
    while True:
        memory.append((sequence[0], sequence[-1]))
        sequence = [t - f for f, t in zip(sequence[:-1], sequence[1:])]
        for i in sequence:
            if i != 0:
                break
        else:
            # Only if all values are 0
            break
    part1 = sum(i[1] for i in memory)

    current_value: int = 0
    for i in memory[::-1]:
        current_value = i[0] - current_value
    return part1, current_value


@time_function
def solve(filename: str):
    input = Input(filename).read()
    with Pool(6) as p:
        result = p.map(extrapolate, input)
    return sum(i[0] for i in result), sum(i[1] for i in result)


if __name__ == '__main__':
    print(solve('input.txt'))
