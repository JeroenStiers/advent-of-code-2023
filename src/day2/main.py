from typing import *
from dataclasses import dataclass, field
from util import Input
from util import StringExtractor
from util import time_function

re_game = StringExtractor(pattern=r'^Game (\d+)')
re_red = StringExtractor(pattern=r'(\d+) red')
re_blue = StringExtractor(pattern=r'(\d+) blue')
re_green = StringExtractor(pattern=r'(\d+) green')


def get_maximum(game: str, regex) -> int:
    return max([int(i.group(1)) for i in regex.finditer(game)])


@time_function
def solve(filename: str):
    input = Input(filename).read()
    game_id_sum: int = 0

    for game in input:
        game_number = get_maximum(game, re_game)

        if get_maximum(game, re_red) > 12:
            continue
        if get_maximum(game, re_green) > 13:
            continue
        if get_maximum(game, re_blue) > 14:
            continue
        game_id_sum += game_number

    return game_id_sum


@time_function
def solve2(filename: str):
    input = Input(filename).read()

    return sum([get_maximum(game, re_red) * get_maximum(game, re_green) * get_maximum(game, re_blue) for game in input])


if __name__ == '__main__':
    print(solve('input.txt'))
    print(solve2('input.txt'))
